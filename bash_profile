export PATH="/usr/local/bin:$PATH"
export PATH="/Applications:$PATH"
export PATH="/usr/local/opt/tcl-tk/bin:$PATH"
export PATH="/usr/local/sbin:$PATH"
export PATH="/usr/local/texlive/2020/bin/x86_64-darwin:$PATH"
export PATH="/usr/local/texbin:$PATH"
export PATH="/usr/local/opt/curl-openssl/bin:$PATH"

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_CTYPE=en_US.UTF-8

if [ -f ~/.bashrc ]; then
  source ~/.bashrc
fi

TIME=$(date "+%H:%M");
DATE=$(date "+%A %B %d");
UPTIME=$(uptime | cut -d',' -f1 | cut -d' ' -f4-);

echo -n "Welcome back "; whoami;
echo "It is $TIME on $DATE; 
$(hostname -s) has been up for $UPTIME";

if [[ $UPTIME == *day* ]] ; then 
    UPINT=$(echo $UPTIME | cut -d" " -f1)
    RED='\033[0;31m'
    NC='\033[0m' # No Color
    if (($UPINT > 5)); then
        echo -e "${RED}uptime exceeds 5 days, consider a reboot.${NC}"
    fi
fi
