#!/usr/bin/env bash

# install packages 

if [ "$(uname)" == "Linux" ]; then

    # Check for vim and install
    if ! dpkg -s vim >/dev/null 2>&1; then
        sudo apt-get install vim
        ln -s ${PWD}/vimrc ~/.vimrc
        # TODO: install vim pluggins
    fi

    # Check for tmux and install
    if ! dpkg -s tmux >/dev/null 2>&1; then
        sudo apt-get install tmux
        ln -s ${PWD}/tmux.conf ~/.tmux.conf
        git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
        ~/.tmux/plugins/tpm/scripts/install_plugins.sh
    fi
fi

# link config files 
BASEDIR=$(dirname $0)
cd $BASEDIR

FILE=~/.vimrc
if [ ! -f "$FILE" ]; then
    ln -sfn ${PWD}/vimrc ~/.vimrc
fi

FILE=~/.tmux.conf
if [ ! -f "$FILE" ]; then
    ln -sfn ${PWD}/tmux.conf ~/.tmux.conf
fi

ln -sfn ${PWD}/aliases ~/.aliases
ln -sfn ${PWD}/bash_profile ~/.bash_profile
ln -sfn ${PWD}/bashrc ~/.bashrc
ln -sfn ${PWD}/vim_headers ~/.vim/headers
ln -sfn ${PWD}/vim_colors ~/.vim/colors

# copy configurations to define locally
FILE=~/.usrps1
if [ ! -f "$FILE" ]; then
    cp ${PWD}/exprompt ~/.usrps1
fi
FILE=~/.vim/local_color_setting.vim
if [ ! -f "$FILE" ]; then
    cp ${PWD}/local_color_setting.vim ~/.vim/local_color_setting.vim
fi

source ~/.bashrc
