" vimrc 
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
" Plugin 'tpope/vim-fugitive'
Plugin 'davidhalter/jedi-vim'
Plugin 'jiangmiao/auto-pairs'
Plugin 'junegunn/goyo.vim'
Plugin 'junegunn/limelight.vim'
Plugin 'junegunn/fzf.vim'
Plugin 'tmux-plugins/vim-tmux-focus-events'
Plugin 'tpope/vim-commentary'
Plugin 'tpope/vim-surround'
Plugin 'vim-syntastic/syntastic' 
Plugin 'preservim/nerdtree'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
" Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
" Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" OPTIONS

set wildmenu 		" Enhance command-line completion
set number 		" Enable line numbers
syntax on 		" Enable syntax highlighting
set mouse+=a 		" Enable mouse scroll
set cursorline 		" Highlight current line
set tabstop=4       " The width of a TAB is set to 4.
                    " Still it is a \t. It is just that
					" Vim will interpret it to be having
					" a width of 4.
set shiftwidth=4    " Indents will have a width of 4
set softtabstop=4   " Sets the number of columns for a TAB
set expandtab       " Expand TABs to spaces
set hlsearch 		" Highlight searches
set ignorecase 		" Ignore case of searches
set noerrorbells 	" Disable error bells
set nostartofline 	" Don’t reset cursor to start of line when moving around.
set ruler 			" Show the cursor position
set backspace=indent,eol,start 		" make backspace work
set clipboard=unnamed   "set clipboard integration
set encoding=utf-8  " utf-8 text 

" vim dim-innactive options
let g:diminactive_enable_focus = 1

" Limelight options
let g:limelight_conceal_ctermfg = 240

" gcal integration
let g:calendar_google_calendar = 1
let g:calendar_google_task = 1

" REMAPPING AND ALIAS
noremap <Leader>y "*y
noremap <Leader>yy "*yy
noremap <Leader>p "*p
noremap <Leader>P "*P
nnoremap ; :
inoremap <C-@> <Esc>
vnoremap <C-@> <Esc>

" COMMANDS AND FUNCTIONS

function Pyhead() abort
    " insert python header from file ~/.vim/headers/python_header.txt
    r~/.vim/headers/python_header.txt
    -d
endfunction

function Texhead() abort
    " insert python header from file ~/.vim/headers/python_header.txt
    r~/.vim/headers/latex_header.txt
    -d
endfunction

" SYNTASTIC SETTINGS
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_python_checkers=['pylint']
let g:syntastic_tex_checkers=['chktex']
let g:syntastic_always_populate_loc_list=1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
" let g:syntastic_debug=1
 
" function Py3()
"     " set python3 syntax for pylint/syntastic
"     let g:syntastic_python_exec = '/usr/local/bin/python3.7'
" endfunction
" 
" call Py3()   " default to Py3

" COLOR AND DESIGN

" shift cursor when in insert mode
let &t_SI.="\e[5 q" "SI = INSERT mode
let &t_SR.="\e[3 q" "SR = REPLACE mode
let &t_EI.="\e[3 q" "EI = NORMAL mode (ELSE)


" check external colorscheme setting
" source ~/.vim/local_color_setting.vim
 
" color and b/g
set background=dark    " Setting dark mode
hi Normal guibg=NONE ctermbg=NONE
hi clear SpellBad
hi SpellBad cterm=underline
hi SpellBad ctermfg=167

